# Teleport

> Save and load current player position or teleport to predefined locations

## Usage

Various common spots are pre-defined and can be teleported to.
Additionally custom locations can be saved.

Command `teleport` (alias `tp`):

| Command             | Description                                                |
|---------------------|------------------------------------------------------------|
| `tp save`           | Save current position as default location (Alias: `tp s`)  |
| `tp save name`      | Save current position as `name`           (Alias: `tp s`)  |
| `tp teleport`       | Teleport to default location              (Alias: `tp t`)  |
| `tp teleport name`  | Teleport to location called `name`        (Alias: `tp t`)  |
| `tp teleport x y z` | Teleport to coordinates `x`, `y`, `z`     (Alias: `tp t`)  |
| `tp delete name`    | Delete location saved as `name`           (Alias: `tp d`)  |
| `tp show current`   | Show current position coordinates                          |
| `tp show name`      | Show saved location coordinates                            |
| `tp list`           | List saved locations                      (Alias: `tp ls`) |
| `tp help`           | Show in-game help message                                  |

## Known Issues

- VMF's command handling sometimes doesn't recognize a command with multiple space separated words when written directly in the chat window. Copy&Paste from an external window instead.
