return {
	run = function()
    local mod_resources = {
      mod_script       = "scripts/mods/teleport/teleport",
      mod_data         = "scripts/mods/teleport/teleport_data",
      mod_localization = "scripts/mods/teleport/teleport_localization"
    }
    local mod = new_mod("teleport", mod_resources)
	end,
	packages = {
		"resource_packages/teleport/teleport"
	}
}
