local mod = get_mod("teleport")

return {
  name = "Teleport",
  description = mod:localize("mod_description"),
  is_toggleable = true,
}
